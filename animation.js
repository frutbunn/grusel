'use strict'

/*
	Atari ST Grusel Demo

	A javascript remake of the famous Atari ST 'Grusel - Scary Demo' by Eckhard Kruse in 1987!
	http://www.eckhardkruse.net/atari_st/index.html
	http://www.eckhardkruse.net/atari_st/grafik.html

	All graphics and sound ripped with an Atari ST emulator, and are the copyright of Eckhard Kruse.

	Javascript remake by David Eggleston (aka frutbunn).
	http://frutbunn.co.uk/

	Using the CODEF framework by Antoine Santo (aka NoNameNo).
	http://codef.santo.fr/
	http://wab.com/

	With some borrowed CODEF stuff from Karl Cullen (aka Mellow Man).
	http://codef.namwollem.co.uk/index_non_ios.html

	This work is licensed under the Creative Commons Attribution 4.0 International License. 
	To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
*/

var animation = function() {

	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-

	var scenery = {

		tile: { // canvasWidth, tileWidth, tileHeight, horizontalTileCount
			owl: new fruCinema.Tile(320, 32, 42)
		},

		drawOwl: function(anim, id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(36, 66);

			var a = scenery.tile.owl.position(anim.current(time) );
			obj.image.owl.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);	
		},

		background: function(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			obj.image.background.draw(canvas, 0, 0);
		}

	}


	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-


	function scene1(data) {
		var DURATION = 8;
		var scene = new fruCinema.Scene(DURATION, "Fade in");

		var tile = { // canvasWidth, tileWidth, tileHeight, horizontalTileCount
			fadeIn: new fruCinema.Tile(640, 640, 400)
		};

		var anim = { // duration, countOrIndices, loop
			fadeIn2: new fruCinema.Animation(DURATION, [0, 2, 3, 4, 5, 6, 7, 8, 9, 10], false)
		};

		var move = { // [coordinates], duration, loop
		};

		function fadeIn(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(0, 0);

			var a = tile.fadeIn.position(anim.fadeIn2.current(time) );
			obj.image.fade.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(fadeIn, "Fade In", data, 0, DURATION);

		return scene;
	}


	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-


	function scene2(data) {
		var DURATION = 19;
		var scene = new fruCinema.Scene(DURATION, "Owl wakes up");

		var OWL_DURATION = 3.5;

		var tile = { // canvasWidth, tileWidth, tileHeight, horizontalTileCount
		};

		var anim = { // duration, countOrIndices, loop
			owl: new fruCinema.Animation(OWL_DURATION, [0,1,2,3,3,3,3,3,3,3,3,     0, 1, 2, 3,  0, 1, 2, 3, 4, 4, 4, 4, 4, 4, 3, 3, 3, 5, 5, 5, 5, 5, 5, 3], false)
		};

		var move = { // [coordinates], duration, loop
		};

		scene.add(scenery.background, "background", data, 0, DURATION);

		function owl(id, obj, time, duration) {
			scenery.drawOwl(anim.owl, id, obj, time, duration);
		} 
		scene.add(owl, "Owl wakes up", data, 0, OWL_DURATION);

		return scene;
	}


	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-


	function scene3(data) {
		var DURATION = 45;
		var scene = new fruCinema.Scene(DURATION, "Ghosts");

		var ZOOM_OUT_DURATION = 3;
		var ZOOM_IN_DURATION = 4;
		var MOVE1_DURATION = 6;
		var MOVE2_DURATION = DURATION-ZOOM_IN_DURATION-MOVE1_DURATION-ZOOM_OUT_DURATION;

		var tile = { // canvasWidth, tileWidth, tileHeight, horizontalTileCount
			ghost: new fruCinema.Tile(640, 128, 72)
		};

		var anim = { // duration, countOrIndices, loop
			zoomIn: new fruCinema.Animation(ZOOM_IN_DURATION, 70, false),
			zoomOut: new fruCinema.Animation(ZOOM_OUT_DURATION, 66, false),
			move1: new fruCinema.Animation(.9, [12, 12, 12, 11, 11, 11, 11, 11, 11, 11], true),

			floatingRight: new fruCinema.Animation(.25, [10, 14], true),
			floatingLeft: new fruCinema.Animation(.25, [9, 13], true)
		};

		var move = { // [coordinates], duration, loop
			zoomInLeft: new fruCinema.Move2(animationData.ghosts.zoomIn.left, ZOOM_IN_DURATION, false),
			zoomInRight: new fruCinema.Move2(animationData.ghosts.zoomIn.right, ZOOM_IN_DURATION, false),
			zoomOutLeft: new fruCinema.Move2(animationData.ghosts.zoomOut.left, ZOOM_OUT_DURATION, false),
			zoomOutRight: new fruCinema.Move2(animationData.ghosts.zoomOut.right, ZOOM_OUT_DURATION, false),

			moveRight: new fruCinema.Move([160,124, 0+32,124], MOVE1_DURATION, false),
			moveLeft: new fruCinema.Move([160,49, 320-32,49], MOVE1_DURATION, false),

			floatingRight: new fruCinema.Move([0+32+16,124, 320-32-16,124], MOVE2_DURATION/8, true),
			floatingLeft: new fruCinema.Move([320-32-16,49, 0+32+16,49], MOVE2_DURATION/8, true),

			ghostsBobbing1: new fruCinema.Move([0,0, 0, 15], .5, true),
			ghostsBobbing2: new fruCinema.Move([0,0, 0, 10], .2, true)
		};


		scene.add(scenery.background, "background", data, 0, DURATION);


		function zoomIn(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var frame = anim.zoomIn.current(time);

			var left = move.zoomInLeft.current(time);
			left.x *=2;
			left.y *=2;
			left.x -= 64;
			left.y -= 36;

			var right = move.zoomInRight.current(time);
			right.x *=2;
			right.y *=2;
			right.x -= 64;
			right.y -= 36;

			var a = tile.ghost.position(animationData.ghosts.zoomIn.frame[frame]);

			a.h-=2; // Chrome fix.

			obj.image.ghost.drawPart(canvas, left.x, left.y, a.x, a.y, a.w, a.h);
			obj.image.ghost.drawPart(canvas, right.x, right.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(zoomIn, "ghosts zoom in", data, 0, ZOOM_IN_DURATION);

		function zoomOut(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var frame = anim.zoomOut.current(time);

			var left = move.zoomOutLeft.current(time);
			left.x *=2;
			left.y *=2;
			left.x -= 64;
			left.y -= 36;

			var right = move.zoomOutRight.current(time);
			right.x *=2;
			right.y *=2;
			right.x -= 64;
			right.y -= 36;

			var a = tile.ghost.position(animationData.ghosts.zoomOut.frame[frame]);

			a.h-=2; // Chrome fix.

			obj.image.ghost.drawPart(canvas, left.x, left.y, a.x, a.y, a.w, a.h);
			obj.image.ghost.drawPart(canvas, right.x, right.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(zoomOut, "ghosts zoom out", data, DURATION-ZOOM_OUT_DURATION, ZOOM_OUT_DURATION);

		function move1(id, obj, time, duration) {
			var canvas = obj.renderBuffer;

			var left = move.moveLeft.current(time);
			left.x *=2;
			left.y *=2;
			left.x -= 64;
			left.y -= 36;

			var right = move.moveRight.current(time);
			right.x *=2;
			right.y *=2;
			right.x -= 64;
			right.y -= 36;

			var p2 = move.ghostsBobbing1.current(time);

			var a = tile.ghost.position(anim.move1.current(time));
			obj.image.ghost.drawPart(canvas, left.x, left.y-p2.y, a.x, a.y, a.w, a.h);
			obj.image.ghost.drawPart(canvas, right.x, right.y-p2.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(move1, "move1 ghosts", data, ZOOM_IN_DURATION, MOVE1_DURATION);
		
		function move2(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var a, i;

			var p2 = move.ghostsBobbing2.current(time);

			var left = move.floatingLeft.current(time);
			if ( (left.x<=40+16 || left.x>=320-40-16) && time>1 ) {
				a = tile.ghost.position(12);
			} else {
				i = move.floatingLeft.index(time);
				a = tile.ghost.position(anim.floatingLeft.current(time));
			}

			if (i) 
				a = tile.ghost.position(anim.floatingRight.current(time));

			left.x *=2;
			left.y *=2;
			left.x -= 64;
			left.y -= 36;
			obj.image.ghost.drawPart(canvas, left.x, left.y-p2.y, a.x, a.y, a.w, a.h);

			// -	-	-

			var right = move.floatingRight.current(time);
			if ( (right.x<=40+16 || right.x>=320-40-16) && time>1 ) {
				a = tile.ghost.position(12);
			} else {
				i = move.floatingRight.index(time);
				a = tile.ghost.position(anim.floatingRight.current(time));
			}

			if (i) 
				a = tile.ghost.position(anim.floatingLeft.current(time));

			right.x *=2;
			right.y *=2;
			right.x -= 64;
			right.y -= 36;
			obj.image.ghost.drawPart(canvas, right.x, right.y-p2.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(move2, "move2 floating ghosts", data, ZOOM_IN_DURATION+MOVE1_DURATION, MOVE2_DURATION);


		return scene;
	}


	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-


	function scene4(data) {
		var DURATION = 94.5;
		var scene = new fruCinema.Scene(DURATION, "Wraiths");

		var OWL_BLINK_DURATION = 4;
		var OWL_BOBING_DURATION = 30;
		var OWL_BOBING2_DURATION = 30-1;
		var OWL_DANCING_DURATION = 29;

		var WRAITHS_WALK_ON_DURATION = 25;
		var WRAITHS_WALK_OFF_DURATION = 31;

		var WRAITHS_DANCING_DURATION = 32-.5;

		var WRAITH_SPACING = 5.4;

		var tile = { // canvasWidth, tileWidth, tileHeight, horizontalTileCount
			wraith: new fruCinema.Tile(470, 94, 114)
		};

		var anim = { // duration, countOrIndices, loop
			owlBlink: new fruCinema.Animation(.8, [3,2,1,0,0,1,2,3,5], false),
			owlBobbing: new fruCinema.Animation(.5, [3,14], true),
			owlDancing: new fruCinema.Animation(2, [15,3,16,3], true),

			wraithsWalkOn: new fruCinema.Animation(1, [0], true),

			wraithsDancing: new fruCinema.Animation(WRAITHS_DANCING_DURATION, [
			 1,
			 2,1,3,1,  2,1,3,1,  2,1,3,1,  2,1,3,
			 4,
			 2,1,3,1,  2,1,3,1,  2,1,3,1,  2,1,3,
			 4,
			 2,1,3,1,  2,1,3,1,  2,1,3,1,  2,1,3, 
			 4,
			 2,1,3,1,  2,1,3,1,  2,1,3,1,  2,1,3,
			 1
			 ], false)
		};

		var move = { // [coordinates], duration, loop
			wraithsWalkOn: new fruCinema.Move([
			 320+50,143, 319,143, 299,137, 272,135, 248,134, 150,133,
			 98,127,    47,126,   17,128,    -138,131
			 ], WRAITHS_WALK_ON_DURATION+WRAITHS_WALK_OFF_DURATION, false),

			wraithsBobbing1: new fruCinema.Move2([0,0, 0, 4], .5, true),
			wraithsBobbing2: new fruCinema.Move2([0,0, 0, 4], .25, true),
		};


		scene.add(scenery.background, "background", data, 0, DURATION);

		function owlBlink(id, obj, time, duration) {
			scenery.drawOwl(anim.owlBlink, id, obj, time, duration);
		} 
		scene.add(owlBlink, "Owl blinks", data, 0, OWL_BLINK_DURATION);

		function owlBobbing(id, obj, time, duration) {
			scenery.drawOwl(anim.owlBobbing, id, obj, time, duration);
		} 
		scene.add(owlBobbing, "Owl bobbing", data, OWL_BLINK_DURATION, OWL_BOBING_DURATION);
		scene.add(owlBobbing, "Owl bobbing2", data, OWL_BLINK_DURATION+OWL_BOBING_DURATION+OWL_DANCING_DURATION, OWL_BOBING2_DURATION);

		function owlDancing(id, obj, time, duration) {
			scenery.drawOwl(anim.owlDancing, id, obj, time, duration);
		} 
		scene.add(owlDancing, "Owl dancing", data, OWL_BLINK_DURATION+OWL_BOBING_DURATION, OWL_DANCING_DURATION);

		function wraithsWalkOn(id, obj, time, duration) {
			var canvas = obj.renderBuffer;

			for(var i=0; i<3; i++) {
				var newTime = time - i*WRAITH_SPACING;

				var p = move.wraithsWalkOn.current(newTime);
				p.x *= 2; p.y *= 2;
				p.x -= 47;
				p.y -= 110+4;

				var p2 = move.wraithsBobbing1.current(time);
				p.y -= p2.y;

				var a = tile.wraith.position(anim.wraithsWalkOn.current(time) );
				obj.image.wraith.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
			}

		} 
		scene.add(wraithsWalkOn, "Wraiths walk on", data, OWL_BLINK_DURATION, WRAITHS_WALK_ON_DURATION);

		function wraithsWalkOff(id, obj, time, duration) {
			var canvas = obj.renderBuffer;

			for(var i=0; i<3; i++) {
				var newTime = time + WRAITHS_WALK_ON_DURATION - i*WRAITH_SPACING;

				var p = move.wraithsWalkOn.current(newTime);
				p.x *= 2; p.y *= 2;
				p.x -= 47;
				p.y -= 110+4;

				var p2 = move.wraithsBobbing1.current(time);
				p.y -= p2.y;

				var a = tile.wraith.position(anim.wraithsWalkOn.current(time) );
				obj.image.wraith.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
			}

		} 
		scene.add(wraithsWalkOff, "Wraiths walk off", data, OWL_BLINK_DURATION+WRAITHS_WALK_ON_DURATION+WRAITHS_DANCING_DURATION, WRAITHS_WALK_OFF_DURATION+6);

		function wraithsDance(id, obj, time, duration) {
			var canvas = obj.renderBuffer;

			for(var i=0; i<3; i++) {
				var newTime = WRAITHS_WALK_ON_DURATION - i*WRAITH_SPACING;

				var p = move.wraithsWalkOn.current(newTime);
				p.x *= 2; p.y *= 2;
				p.x -= 47;
				p.y -= 110+4;

				var p2 = move.wraithsBobbing2.current(time);
				p.y -= p2.y;

				var a = tile.wraith.position(anim.wraithsDancing.current(time) );
				obj.image.wraith.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
			}

		} 
		scene.add(wraithsDance, "Wraiths dancing", data, OWL_BLINK_DURATION+WRAITHS_WALK_ON_DURATION, WRAITHS_DANCING_DURATION);

		function leaves(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			obj.image.leaves.draw(canvas, 0, 128);
		} 
		scene.add(leaves, "leaves", data, 0, DURATION);


		return scene;
	}


	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-


	function scene5(data) {
		var DURATION = 129-2;
		var scene = new fruCinema.Scene(DURATION, "Skeleton");

		var ENDING_DURATION = 2;
		var COFFIN_DURATION = 10;

		var SKELETON1_DURATION = 2;
		var SKELETON2_DURATION = 6-1;

		var SKELETON3_DURATION = 11;
		var SKELETON3B_DURATION = 6;
		var SKELETON3B_DELTA = 2.7;

		var SKELETON4_DURATION = 4;

		var SKELETON_ANIM_DURATION = 55;
		var SKELETON_ANIM_B_DURATION = 7.765;
		var SKELETON_ANIM_C_DURATION = 7.5;

		var SKELETON5_DURATION = 17;

		var BAT_DURATION = 4;

		var tile = { // canvasWidth, tileWidth, tileHeight, horizontalTileCount
			coffin: new fruCinema.Tile(684, 228, 144),

			skelAnim: new fruCinema.Tile(730, 146, 140),
			skelHead: new fruCinema.Tile(448, 64, 64),
			skelAnim2: new fruCinema.Tile(800, 160, 160),

			bat: new fruCinema.Tile(192, 96, 22)
		};

		var anim = { // duration, countOrIndices, loop
			coffin: new fruCinema.Animation(9, 17*3, false),
			owlWings: new fruCinema.Animation(2, [17,3,18,3], true),
			owlBobbing: new fruCinema.Animation(.5, [3,14], true),
			owlDancing: new fruCinema.Animation(2, [15,3,16,3], true),

			owlClosesEyes: new fruCinema.Animation(.4, [3,2,1,0], false),

			skeleton1: new fruCinema.Animation(SKELETON1_DURATION, [0,1,2,3], false),
			skeleton2: new fruCinema.Animation(.5, [4,5], true),
			skeleton3: new fruCinema.Animation(SKELETON3_DURATION, animationData.skelWalkRight, false),
			skeleton3b: new fruCinema.Animation(.5, [3,4,5,6], true),

			skeleton4: new fruCinema.Animation(SKELETON4_DURATION, [6,7,36,14,6,7,15,19,6,7,6,7,6,7], false),
			skeleton5: new fruCinema.Animation(SKELETON5_DURATION, animationData.skelWalkLeft, false),

			skelAnim: new fruCinema.Animation(SKELETON_ANIM_DURATION, animationData.skelAnim, false),
			skelAnimB: new fruCinema.Animation(.6, [0,1,2,3], true),

			bat: new fruCinema.Animation(.5, 2, true),

			skeleton6: new fruCinema.Animation(SKELETON1_DURATION, [3,2,1,0], false)
		};

		var move = { // [coordinates], duration, loop
			skeleton3b: new fruCinema.Move([108,202, 100,206, 75,204, 50,240,  55,220,  95,100,  108,202], 3.62-1, false),

			skeleton4: new fruCinema.Move([56, 206-8, 260-2, 166+10], SKELETON4_DURATION-.5, false),

			skelAnimB: new fruCinema.Move([170,108, 170,74, 170,108], SKELETON_ANIM_B_DURATION, false),
			skelAnimC: new fruCinema.Move([0,0, 64,0, 0,0], SKELETON_ANIM_C_DURATION, false),

			bat: new fruCinema.Move(animationData.bat, BAT_DURATION, false),

			skeleton5: new fruCinema.Move([260, 166+10,   56, 206-8], 3.5, false)
		};

		scene.add(scenery.background, "background", data, 0, DURATION);

		function owlWings(id, obj, time, duration) {
			scenery.drawOwl(anim.owlWings, id, obj, time, duration);
		} 
		scene.add(owlWings, "Owl wings", data, COFFIN_DURATION-.3+SKELETON1_DURATION, 20);


		function owlBobbing(id, obj, time, duration) {
			scenery.drawOwl(anim.owlBobbing, id, obj, time, duration);
		} 
		scene.add(owlBobbing, "Owl bobbing", data, 32, 23);


		function owlDancing(id, obj, time, duration) {
			scenery.drawOwl(anim.owlDancing, id, obj, time, duration);
		} 
		scene.add(owlDancing, "Owl dancing", data, 32+23, 49);


		function owlClosesEyes(id, obj, time, duration) {
			scenery.drawOwl(anim.owlClosesEyes, id, obj, time, duration);
		} 
		scene.add(owlClosesEyes, "Owl wakes up", data, 125+1.5, DURATION);


		function coffinOpen(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(8, 224);

			var a = tile.coffin.position(anim.coffin.current(time) );
			obj.image.lid.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);

			if (time>=COFFIN_DURATION-1.5 && time<=COFFIN_DURATION) {
				p = new fruCinema.Coordinate(56, 206);
				a = tile.skelAnim2.position(0);
				obj.image.skelAnim2.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
			}
		} 
		scene.add(coffinOpen, "coffin opens", data, 0, DURATION-COFFIN_DURATION);


		function skeleton1(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(56, 206);

			var a = tile.skelAnim2.position(anim.skeleton1.current(time) );
			obj.image.skelAnim2.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(skeleton1, "skeleton out of coffin", data
		 , COFFIN_DURATION, SKELETON1_DURATION);


		function skeleton2(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(56, 206-8);

			var a = tile.skelAnim2.position(anim.skeleton2.current(time) );
			obj.image.skelAnim2.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(skeleton2, "skeleton bobbing", data
		 , COFFIN_DURATION+SKELETON1_DURATION, SKELETON2_DURATION);


		function skeleton3(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(56, 206-8);

			var a = tile.skelAnim2.position(anim.skeleton3.current(time) );
			obj.image.skelAnim2.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(skeleton3, "skeleton bobbing2", data
		 , COFFIN_DURATION+SKELETON1_DURATION+SKELETON2_DURATION, SKELETON3_DURATION);


		function skeleton3b(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(108, 202);

			var moveOffset = 2; //1
			if (time>moveOffset)
				p = move.skeleton3b.current(time-moveOffset);

			var a = tile.skelHead.position(anim.skeleton3b.current(time) );
			obj.image.skelHead.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(skeleton3b, "skeleton bobbing2 head", data, 19.2, 4.62);


		function skeleton4(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = move.skeleton4.current(time);

//			p.x = Math.floor(p.x/8)*8;
//			p.y = Math.floor(p.y/8)*8;

			var a = tile.skelAnim2.position(anim.skeleton4.current(time) );
			obj.image.skelAnim2.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(skeleton4, "skeleton4 Walk to center of screen", data
		 , COFFIN_DURATION+SKELETON1_DURATION+SKELETON2_DURATION+SKELETON3_DURATION, SKELETON4_DURATION);		


		function skeletonAnim(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(260, 166-50+20+62);

			var moveOffset = 32;
			if (time>moveOffset) {
				var p2 = move.skelAnimC.current(time-moveOffset);
				var displace = p2.x;
//				var displace = Math.floor(p2.x/8)*8;
				p.x+=displace;
			}

			var a = tile.skelAnim.position(anim.skelAnim.current(time) );
			obj.image.skelAnim.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(skeletonAnim, "skeleton anim", data, COFFIN_DURATION+SKELETON1_DURATION+SKELETON2_DURATION+SKELETON3_DURATION+SKELETON4_DURATION, SKELETON_ANIM_DURATION);

		function skeletonAnimB(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = move.skelAnimB.current(time);
			p.x *= 2; p.y *= 2;
			p.x -= 32;
			p.y -= 32;

			var a = tile.skelHead.position(anim.skelAnimB.current(time) );
			obj.image.skelHead.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(skeletonAnimB, "skeleton anim head", data, 55.75, SKELETON_ANIM_B_DURATION);


		function skeleton5(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = move.skeleton5.current(time);

//			p.x = Math.floor(p.x/8)*8;
//			p.y = Math.floor(p.y/8)*8;

			var a = tile.skelAnim2.position(anim.skeleton5.current(time) );
			obj.image.skelAnim2.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(skeleton5, "skeleton5 Walk back to coffin", data
		 , COFFIN_DURATION+SKELETON1_DURATION+SKELETON2_DURATION+SKELETON3_DURATION+SKELETON4_DURATION+SKELETON_ANIM_DURATION, SKELETON5_DURATION);		


		function skeleton6(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(56, 206);

			var a = tile.skelAnim2.position(anim.skeleton6.current(time) );
			obj.image.skelAnim2.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(skeleton6, "skeleton sit down in coffin", data
		 , COFFIN_DURATION+SKELETON1_DURATION+SKELETON2_DURATION+SKELETON3_DURATION+SKELETON4_DURATION+SKELETON_ANIM_DURATION+SKELETON5_DURATION, SKELETON1_DURATION);


		function coffinClose(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(8, 224);

			var a = tile.coffin.position(anim.coffin.current(9-time) );
			obj.image.lid.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);

			if (time<=1.5) {
				p = new fruCinema.Coordinate(56, 206);
				a = tile.skelAnim2.position(0);
				obj.image.skelAnim2.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
			}
		} 
		scene.add(coffinClose, "coffin closes", data, 
		COFFIN_DURATION+SKELETON1_DURATION+SKELETON2_DURATION+SKELETON3_DURATION+SKELETON4_DURATION+SKELETON_ANIM_DURATION+SKELETON5_DURATION+SKELETON1_DURATION, DURATION);


		function batRight(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = move.bat.current(time);
			p.x *=2; p.y*=2;
			p.x-=48; p.y-=11;

			var a = tile.bat.position(anim.bat.current(time) );
			obj.image.bat.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
		}
		function batLeft(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = move.bat.current(BAT_DURATION-time);
			p.x *=2; p.y*=2;
			p.x-=48; p.y-=11;

			var a = tile.bat.position(anim.bat.current(BAT_DURATION-time) );
			obj.image.bat.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
		}
		scene.add(batRight, "bat0", data, 43.5, BAT_DURATION);
		scene.add(batRight, "bat1", data, 51, BAT_DURATION);
		scene.add(batLeft, "bat2", data, 63, BAT_DURATION);//
		scene.add(batRight, "bat3", data, 68, BAT_DURATION);
		scene.add(batRight, "bat4", data, 75, BAT_DURATION);
		scene.add(batRight, "bat5", data, 91, BAT_DURATION);
		scene.add(batRight, "bat6", data, 100, BAT_DURATION);
		return scene;
	}


	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-


	function scene6(data) {
		var DURATION = 11;
		var BLACK_DURATION = 3;

		var scene = new fruCinema.Scene(DURATION, "Fade out");

		var tile = { // canvasWidth, tileWidth, tileHeight, horizontalTileCount
			fadeOut: new fruCinema.Tile(640, 640, 400)
		};

		var anim = { // duration, countOrIndices, loop
			fadeOut: new fruCinema.Animation(DURATION-BLACK_DURATION, [10, 9, 8, 7, 6, 5, 4, 3, 2, 0, 1], false)
		};

		var move = { // [coordinates], duration, loop
		};

		function fadeOut(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(0, 0);

			var a = tile.fadeOut.position(anim.fadeOut.current(time) );
			obj.image.fade.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(fadeOut, "Fade Out", data, 0, DURATION-BLACK_DURATION);

		return scene;
	}


	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-


	function make(data) {
		film.add(scene1(data) );
		film.add(scene2(data) );
		film.add(scene3(data) );
		film.add(scene4(data) );
		film.add(scene5(data) );
		film.add(scene6(data) );
	}

	return {
		make: make
	}
}();
